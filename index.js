/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

exports.stripPrivateProperties = (privateFieldList, data) =>
  data.map((item) => {
    privateFieldList.forEach((privField) => {
      // eslint-disable-next-line no-param-reassign
      delete item[privField];
    });
    return item;
  });

exports.excludeByProperty = (propertyToExclude, data) => {
  if (typeof propertyToExclude !== 'string') {
    return data;
  }
  return data.filter(item =>
    !Object.prototype.hasOwnProperty.call(item, propertyToExclude));
};

exports.sumDeep = (data, propToSum = 'val') => data.map((item) => {
  const summedObject = {};
  Object.keys(item).forEach((propertyName) => {
    summedObject[propertyName] = (Array.isArray(item[propertyName]) ? item[propertyName] : [])
      .map(i => i[propToSum])
      .reduce((prev, current) => prev + (current || 0), 0);
  });
  return summedObject;
});

exports.applyStatusColor = (associations, statuses) => {
  const statusColorMapped = {};
  Object.entries(associations).forEach((item) => {
    const color = item[0];
    const statusCodes = item[1];
    statusCodes.forEach((code) => {
      statusColorMapped[code] = color;
    });
  });

  return statuses.flatMap((item) => {
    const color = statusColorMapped[item.status];
    return color ? [{ ...item, color }] : [];
  });
};

exports.createGreeting = (greetingFn, text) => name => greetingFn(text, name);

exports.setDefaults = (defaultValues = {}) => obj => ({ ...defaultValues, ...obj });

exports.fetchUserByNameAndUsersCompany =
    async (username, { fetchStatus, fetchUsers, fetchCompanyById }) => {
      const status = await fetchStatus();
      const userList = await fetchUsers();
      const user = userList.find(({ name }) => name === username) || {};
      const company = await fetchCompanyById(user.companyId);

      return {
        company,
        status,
        user,
      };
    };
